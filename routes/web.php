<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/buku', 'BukuController@index');
Route::get('buku/tambah', 'BukuController@tambah');
Route::post('/buku/store', 'BukuController@store');
Route::get('{id}/edit/', 'BukuController@edit');
Route::put('/buku/update/{id}', 'BukuController@update');
Route::delete('/buku/hapus/{id}', 'BukuController@destroy');
//Route::get('/buku', 'WebController@index');