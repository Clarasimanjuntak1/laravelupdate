<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet">
        <title>Edit</title>
    </head>
    <body>
        <div class="container">
            <div class="card mt-5">
                <div class="card-body">
                    <a href="/buku/edit" class="btn btn-primary">Kembali</a>
                    <br/>
                    <br/>
                    

                    <form method="post" action="/buku/update/{{ $buku->id }}">

                        {{ csrf_field() }}
                        {{ method_field('PUT') }}

                        <div class="form-group">
                            <label>Id</label>
                            <input type="text" name="id" class="form-control" placeholder="" value="{{$buku->id}}">

                            @if($errors->has('id'))
                                <div class="text-danger">
                                    {{ $errors->first('id')}}
                                </div>
                            @endif

                        </div>

                        <div class="form-group">
                            <label>Judul</label>
                            <input name="judul" class="form-control" placeholder="" value="{{$buku->judul}}"></input>

                             @if($errors->has('judul'))
                                <div class="text-danger">
                                    {{ $errors->first('judul')}}
                                </div>
                            @endif

                        </div>
                        <div class="form-group">
                            <label>Penulis</label>
                            <input name="penulis" class="form-control" placeholder="" value="{{$buku->penulis}}"></input>

                             @if($errors->has('penulis'))
                                <div class="text-danger">
                                    {{ $errors->first('penulis')}}
                                </div>
                            @endif

                        </div>
                        <div class="form-group">
                            <label>Penerbit</label>
                            <input name="penerbit" class="form-control" placeholder="" value="{{$buku->penerbit}}"></input>

                             @if($errors->has('penerbit'))
                                <div class="text-danger">
                                    {{ $errors->first('penerbit')}}
                                </div>
                            @endif

                        </div>
                        <div class="form-group">
                            <label>Jenis</label>
                            <input name="jenis" class="form-control" placeholder="" value="{{$buku->jenis}}"></input>

                             @if($errors->has('jenis'))
                                <div class="text-danger">
                                    {{ $errors->first('jenis')}}
                                </div>
                            @endif

                        </div>

                        <div class="form-group">
                            <input type="submit" class="btn btn-success" value="Simpan">
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </body>
</html>