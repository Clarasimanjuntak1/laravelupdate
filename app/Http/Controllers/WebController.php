<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WebController extends Controller
{
    public  function index(){
        $buku = Buku::all();
        return view('buku', ['buku' => $buku]);
    }
}
