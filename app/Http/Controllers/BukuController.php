<?php

namespace App\Http\Controllers;

use App\Buku;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BukuController extends Controller
{
    public function index()
    {
        //mengambil data buku
        $buku = Buku::all();

        // mengambil data dari tabel pegawai
        $buku = DB::table('databuku')->paginate(10);

        //mengirim data buku ke view buku

        return view('buku', ['buku' => $buku]);
    }

    public function tambah()
    {
        return view('create');
    }

    public function store(Request $request)
    {
        $this -> validate($request, [
            'id' => 'required',
            'judul' => 'required',
            'penulis' => 'required',
            'penerbit' => 'required',
            'jenis' => 'required',
        ]);

        Buku::create([
            'id' => $request->id,
            'judul' => $request->judul,
            'penulis' => $request->penulis,
            'penerbit' => $request->penerbit,
            'jenis' => $request->jenis
        ]);
        return redirect('/buku');
    }

    public function edit($id)
    {
        $buku = Buku::find($id);
        return view('edit', ['buku' => $buku], compact('id'));
    }

    public function update($id, Request $request)
    {
        $this -> validate($request, [
            'id' => 'required',
            'judul' => 'required',
            'penulis' => 'required',
            'penerbit' => 'required',
            'jenis' => 'required'
        ]);

        $buku = Buku::find($id);
        $buku -> id = $request -> id;
        $buku -> judul = $request -> judul;
        $buku -> penulis = $request -> penulis;
        $buku -> penerbit = $request -> penerbit;
        $buku -> jenis = $request -> jenis;
        $buku -> save();
        return redirect('/buku');
    }

    public function destroy($id)
    {
        Buku::destroy($id);
       // return redirect('/buku') 
       // $buku = Buku::find($id);
       // $buku -> delete();
        return redirect('/buku') ->with('status', 'Data Buku berhasil dihapus');
    }
}
